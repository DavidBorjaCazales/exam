import unittest
from Products import Products

class TestProduct(unittest.TestCase):


    def test_instance(self):
        product = Products('https://example.com/api/products', {})
        self.assertIsInstance(product, Products)

    def test_getResponseWrong(self):
        product = Products('https://example.com/api/products', {})
        self.assertEqual(product.get(), "message : Service Products not working protertly")

    def test_getResponseCorrect(self):
        product = Products('https://example.com/api/products', {})
        self.assertEqual(product.get(), [])

    def test_execute(self):
        product = Products('https://example.com/api/products', {})
        self.assertEqual(product.execute(), [])

if __name__ == '__main__':
    unittest.main()