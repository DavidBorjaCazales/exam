from HTTPBase import HTTPBase
import requests

class Products(HTTPBase):
	out = []

	def __init__(self, url, data):
		self.url = url
		self.data = data

	def get(self):
		try:
			r = requests.get(self.url, params=self.data)
			if (r.status_code != 200):
				raise Exception("Service Products not working protertly")
			self.out = r.json()
			return r.json()
		except Exception, e:
			return "message : " + str(e)

	def execute(self):
		result = []

		try:
			jsonResponse  = self.get()
			products = jsonResponse.data

			for product in products:
				if (product["rating"] > 4.0):
					result = result + [product]
			return result
		except Exception, e:
			return "message : "+str(e)


	def post(self):
		pass

	def delete(self):
		pass

	def put(self):
		pass