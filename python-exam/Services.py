from Products import Products

class Services:
	instance = None

	def __init__(self, service, url, data):
		if service == "Products":
			self.instance =  Products(url, data)

	def execute(self):
		return self.instance.execute()