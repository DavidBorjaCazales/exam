from abc import ABCMeta, abstractmethod

class HTTPBase:
	__metaclass__ = ABCMeta

	@abstractmethod
	def get(self):
		pass
	@abstractmethod		
	def post(self):
		pass
	@abstractmethod
	def delete(self):
		pass
	@abstractmethod
	def put(self):
		pass